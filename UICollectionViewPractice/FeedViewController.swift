//
//  FeedViewController.swift
//  UICollectionViewPractice
//
//  Created by usher on 2018/6/6.
//  Copyright © 2018年 usher. All rights reserved.
//

import Foundation
import UIKit

let cellId = "cellId"
class FeedViewController: UICollectionViewController{
    
    override func viewDidLoad() {
        collectionView?.alwaysBounceVertical = true
        collectionView?.backgroundColor = UIColor(white: 0.95, alpha: 1)
        collectionView?.register(FeedCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 10
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! FeedCell
        
        return cell
    }
    
}
