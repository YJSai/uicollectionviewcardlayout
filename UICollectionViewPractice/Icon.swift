//
//  Icon.swift
//  UICollectionViewPractice
//
//  Created by usher on 2018/6/6.
//  Copyright © 2018年 usher. All rights reserved.
//

import Foundation

struct Icon {
    var name = ""
    var price: Double = 0.0
    var isFeatured = false
    var description:String?
    init(name: String, price: Double, isFeatured: Bool) {
        self.name = name
        self.price = price
        self.isFeatured = isFeatured
    }
}
