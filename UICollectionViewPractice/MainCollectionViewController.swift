//
//  MainCollectionViewController.swift
//  UICollectionViewPractice
//
//  Created by usher on 2018/6/6.
//  Copyright © 2018年 usher. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class MainCollectionViewController: UICollectionViewController {

    var selectMode = false
    var selectedIcons: [Icon] = []
    private var iconSet: [Icon] = [Icon(name: "candle", price: 3.99, isFeatured: false),Icon(name: "cat", price: 2.99, isFeatured: true), Icon(name: "dribbble", price: 1.99, isFeatured: false), Icon(name: "ghost", price: 4.99, isFeatured: false),Icon(name: "hat", price: 2.99, isFeatured: false),Icon(name: "owl", price: 5.99, isFeatured: false),Icon(name: "pot", price: 1.99, isFeatured: false),Icon(name: "pumkin", price: 0.99, isFeatured: false),Icon(name: "rip", price: 7.99, isFeatured: false),Icon(name: "skull", price: 8.99, isFeatured: false),Icon(name: "sky", price: 0.99, isFeatured: false),Icon(name: "toxic", price: 2.99, isFeatured: false),Icon(name: "ic_book", price: 2.99, isFeatured: false),Icon(name: "ic_backpack", price: 3.99, isFeatured: false),Icon(name: "ic_camera", price: 4.99, isFeatured: false),Icon(name: "ic_coffee", price: 3.99, isFeatured: true),Icon(name: "ic_glasses", price: 4.99, isFeatured: false),Icon(name: "ic_ice_cream", price: 4.99, isFeatured: false),Icon(name: "ic_smoking_pipe", price: 6.99, isFeatured: false),Icon(name: "ic_vespa", price: 9.99, isFeatured: false)]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
//        self.collectionView!.register(IconCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func shareButtonTapped(_ sender: UIBarButtonItem) {
        
        if !selectMode {
            sender.title = "done"
            collectionView?.allowsMultipleSelection = true
            selectMode = true
        } else {
            sender.title = "share"
            collectionView?.allowsMultipleSelection = false
            selectMode = false
        }
        
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return iconSet.count
    }

    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! IconCollectionViewCell
    
        let icon = iconSet[indexPath.row]
        cell.iconImageView.image = UIImage(named: icon.name)
        cell.iconPriceLabel.text = "$\(icon.price)"
        // Configure the cell
    
        cell.backgroundView = icon.isFeatured ? UIImageView(image: #imageLiteral(resourceName: "feature-bg")) : nil
        cell.selectedBackgroundView = UIImageView(image: UIImage.init(named: "icon-selected"))
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard selectMode else {
            return
        }
        let selectedIcon = iconSet[indexPath.row]
        selectedIcons.append(selectedIcon)
        
        
    }

    override func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        // Check if the sharing mode is enabled, otherwise, just leave this method
        guard selectMode else {
            return
        }
        
        let deSelectedIcon = iconSet[indexPath.row]
        
        // Find the index of the deselected icon. Here we use the index method and pass it
        // a closure. In the closure, we compare the name of the deselected icon with all
        // the items in the selected icons array. If we find a match, the index method will
        // return us the index for later removal.
        if let index = selectedIcons.index(where: { $0.name == deSelectedIcon.name }) {
            selectedIcons.remove(at: index)
        }
    }
//    override func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
//        guard selectMode else {
//            return
//        }
//        let deSelected = iconSet[indexPath.row]
//        if let index = selectedIcons.index(where: { $0.name == deSelected.name }) {
//            selectedIcons.remove(at: index)
//        }
//        debugPrint("123")
//    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "showIconDetail" {
            if selectMode {
                return false
            }
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard !selectMode else {
            return
        }
        
        if segue.identifier == "showIconDetail" {
            if let indexPaths = collectionView?.indexPathsForSelectedItems {
                let destinationController = segue.destination as! ViewController
                destinationController.icon = self.iconSet[indexPaths.first!.row]
                collectionView?.deselectItem(at: indexPaths[0], animated: false)
            }
        }
    }
    
    @IBAction func unwindToHome(segue: UIStoryboardSegue) {
        
    }
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
