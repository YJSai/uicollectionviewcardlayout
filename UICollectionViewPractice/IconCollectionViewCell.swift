//
//  IconCollectionViewCell.swift
//  UICollectionViewPractice
//
//  Created by usher on 2018/6/6.
//  Copyright © 2018年 usher. All rights reserved.
//

import UIKit

class IconCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var iconPriceLabel: UILabel!
}
