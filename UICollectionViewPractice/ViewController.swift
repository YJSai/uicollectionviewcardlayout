//
//  ViewController.swift
//  UICollectionViewPractice
//
//  Created by usher on 2018/6/6.
//  Copyright © 2018年 usher. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var icon: Icon?
    
    @IBOutlet weak var iconImageView: UIImageView! {
        didSet {
            iconImageView.image = UIImage(named: icon?.name ?? "")
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.text = icon?.name
        }
    }
    
    @IBOutlet weak var priceLabel: UILabel! {
        didSet {
            priceLabel.text = "$\(icon!.price)"
        }
    }
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.text = icon?.description ?? "no description"
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

//    @IBAction func unwindToHome(segue: UIStoryboardSegue) {
//        
//    }

}

