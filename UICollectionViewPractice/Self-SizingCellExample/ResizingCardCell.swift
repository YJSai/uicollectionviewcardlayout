//
//  CardCell.swift
//  CollectionViewAutoSizingTest
//
//  Created by ThinkCloud on 2018/7/2.
//  Copyright © 2018年 Wasin Wiwongsak. All rights reserved.
//

import UIKit

class ResizingCardCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        setup()
//    }
//
//
//
    
    
    required init?(coder aDecoder: NSCoder) {
//        let screenWidth = UIScreen.main.bounds.size.width
//        widthConstraint.constant = screenWidth - (2 * 12)
        super.init(coder: aDecoder)

        
        
        //self.translatesAutoresizingMaskIntoConstraints = false
        //fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    
    
    override func willMove(toSuperview newSuperview: UIView?) {
        let screenWidth = UIScreen.main.bounds.size.width
        debugPrint(screenWidth)
        setup()
        
        
    }
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addShadow()
    }
    
    func addShadow() -> Void {
        self.contentView.layer.borderWidth = 1
        self.contentView.backgroundColor = UIColor.white
        self.contentView.layer.cornerRadius = 5
        self.contentView.clipsToBounds = true
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true
        self.layer.shadowColor = UIColor.gray.cgColor
        
        self.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        let contentInsets = UIEdgeInsetsMake(10, 5, 0, 5)
        let shadowRect = CGRect(x: 0, y: 0, width: self.contentView.frame.size.width, height: self.contentView.frame.size.height)
        let insetRect = UIEdgeInsetsInsetRect(shadowRect, contentInsets)
        self.layer.shadowRadius = 4.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: insetRect,cornerRadius: self.contentView.layer.cornerRadius).cgPath
    }
    
    func setup() -> Void {
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        let screenWidth = UIScreen.main.bounds.size.width
        if let constraint = widthConstraint {
            constraint.constant = screenWidth - (2 * 12)
        }
    }
}
